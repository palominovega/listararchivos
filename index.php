<?php

require_once "vendor/autoload.php";

# Indicar que usaremos el IOFactory
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

use PhpOffice\PhpSpreadsheet\Spreadsheet;


function generarExcel(){

	$documento = new Spreadsheet();
	$hojaActual = $documento->getSheet(0);
	
	// $lst = showFiles("../ArchivosMimp/frontend/src/app");//frontend
	$lst = showFiles("../ArchivosMimp/backend/main/java"); //backend
	
	$hojaActual->setCellValue("A1", 'Archivo');
	$hojaActual->setCellValue("B1", 'Extensión');
	$hojaActual->setCellValue("C1", 'Creación');
	$hojaActual->setCellValue("D1", 'Modificación');
	$hojaActual->setCellValue("E1", 'Autor');

	$indiceFila = 1; 
  
	if(count($lst)>0){

		/// INSERTAR REGISTROS AL EXCEL
		foreach($lst as $row){
      $file = explode("#", $row);

			$indiceFila ++;

			$hojaActual->setCellValue("A".$indiceFila, $file[0]);
			$hojaActual->setCellValue("B".$indiceFila, ".".$file[1]);
			$hojaActual->setCellValue("C".$indiceFila, "05/01/2021");
			$hojaActual->setCellValue("D".$indiceFila, "05/01/2021");
			$hojaActual->setCellValue("E".$indiceFila, "Gis Corporativo");
						
			//dar formato a las celdas
			// $hojaActual->getStyle("F".$indiceFila)->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DDMMYYYY);
			$hojaActual->getStyle("A".$indiceFila)->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_TEXT);
			$hojaActual->getStyle("G".$indiceFila)->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_TEXT);

		}

	}else{
		echo "Sin registros";
	}

	//ancho automatico y negrita a la cabecera
	foreach(range('A','E') as $columnID) {
		$hojaActual->getColumnDimension($columnID)->setAutoSize(true);
		$hojaActual->getStyle($columnID.'1')->getFont()->setBold(true);
	}

	$fileName = $_SERVER['DOCUMENT_ROOT'].'/listarArchivosPHP/nuevoArchivo.xlsx';
	
	eliminarArchivo($fileName);

	$writer = new Xlsx($documento);
	$writer->save($fileName);

	if( file_exists($fileName ) && is_file($fileName) ) {
		echo "success";
	}
}

generarExcel();

/**
 * https://www.jose-aguilar.com/blog/recorrer-directorio-de-archivos-con-php/
 * https://blog.trescomatres.com/2012/01/ejemplos-php-recorrer-ficheros-de-un-directorio/
 */
function showFiles($path, &$files = array()) {

  $dir = opendir($path);

  while ($current = readdir($dir)) {
    if ($current != "." && $current != "..") {

      if (is_dir($path . '/' . $current)) {
        showFiles($path . '/' . $current . '/', $files);
      } else {
        $nombreDelArchivo = $current;
        $extension = pathinfo($nombreDelArchivo, PATHINFO_EXTENSION);
        $files[] = $current.'#'.$extension;
      }
    }
  }

  // echo '<h2>' . $path . '</h2>';
  // echo '<ul>';
  // for ($i = 0; $i < count($files); $i++) {
  //   echo '<li>' . $files[$i] . "</li>";
  // }
  // echo '</ul>';
  // echo('<pre>');
  //   var_dump($files);
  // echo('</pre>');

  return $files;

}

function eliminarArchivo($fichero_local){
	
	if( file_exists($fichero_local ) && is_file($fichero_local) ) {
		unlink($fichero_local);
	}

}

/**
 * https://www.it-swarm-es.com/es/php/listar-todos-los-archivos-y-carpetas-en-un-directorio-con-php-funcion-recursiva/1048502006/
 */
function getDirContents($dir, &$results = array()){
  $files = scandir($dir);

  foreach($files as $key => $value){
      $path = realpath($dir.DIRECTORY_SEPARATOR.$value);
      if(!is_dir($path)) {
          $results[] = $path;
      } else if($value != "." && $value != "..") {
          getDirContents($path, $results);
          // $results[] = $path;
      }
  }

  return $results;
}


  // echo('<pre>');
  //   var_dump(getDirContents("../ArchivosMimp/frontend/src/app"));
  // echo('</pre>');